import { Router } from 'express'
import { TrocaController } from './troca.controller'

class TrocaRouter {

    private TrocaController: TrocaController
    public TrocaRouter = Router()

    public constructor() {
        this.TrocaController = new TrocaController()
        this.setUp()
    }

    public setUp(): void {
        this.TrocaRouter.get(`${process.env.API_BASE_URL}/`, async (_req, res) => {
            return res
                .status(200)
                .json('Rota Raiz do projeto')
        })
        this.TrocaRouter
            .get(`${process.env.API_BASE_URL}/teste`,
                async (_req, res) => {
                    return res
                        .status(200)
                        .json(this.TrocaController.teste)
                }
            )
        this.TrocaRouter
            .get(`${process.env.API_BASE_URL}/usuario`,
                this.TrocaController.getUsuario)
    }
}

export default new TrocaRouter().TrocaRouter
