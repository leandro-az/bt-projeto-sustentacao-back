import delay from 'delay'
import {
    searchMatricula,
    getLikedServe,
    updateDateUnidateDest,
    getJobsByUnit,
    putJobsByUnit,
    getBoolPacketGenerated,
    jobGeraPacket,
    execProcTrocaUnid
} from '../Mssql/mssql_repository'



export class TrocaService {
    /**
     * name
     */
    public resolveTroca(): string {
        // Aqui teriam as operaçoes passo a passo para realizar a troca
        // Exemplo const resultado = await functionDefaultExample(qry)
        try {
            return 'sucesso'
        } catch (error) {
            console.log(error)
            return 'falha'
        }
    }
    public async getUsuario(matricula: number, unidadeDest: string): Promise<any> {
        try {
            const resultado: any[] = await searchMatricula(matricula)
            let inUnidade = false
            let sqUnidade = 0
            let strUnidDest = ''
            if (!resultado || !resultado[0]) {
                return -1
            } else {
                // Regra número 1, para fazer troca precisa ter mais de uma unidade no result
                let arrayElements = resultado[0]
                if (arrayElements.length <= 1) {
                    console.log('Result: 1')
                    return 1
                }
                let qtd_prin = 0
                for (let index = 0; index < arrayElements.length; index++) {
                    const element = arrayElements[index]
                    if (element.CLUE_IN_PRINCIPAL) {
                        qtd_prin += 1
                    }
                    if (element.UNIDADE === unidadeDest) {
                        inUnidade = true
                        sqUnidade = element.UNID_SQ_UNIDADE
                        strUnidDest = element.UNIDADE
                    }
                }
                // Regra número 2, para fazer a troca só pode haver uma única unidade marcada como principal
                if (qtd_prin > 1) {
                    console.log('Result: 2')
                    return 2
                }

                // Regra 3, Verificar se dentro de arrayelements a unidade de destino existe
                if (!inUnidade) {
                    console.log('Result: 3')
                    return 3
                }
                // Regra 4 recuperar Linked Server
                const linkedServ = await getLikedServe(strUnidDest)
                if (!linkedServ) {
                    console.log('Result: 4')
                    return 4
                }
                const statusUpdate = await updateDateUnidateDest(matricula, sqUnidade, linkedServ.LINKED_SERVER)
                // Se não Conseguiu atualizar
                if (!statusUpdate) {
                    console.log('Result: 5')
                    return 5
                }
                const jobsList = await getJobsByUnit(unidadeDest)
                arrayElements = jobsList[0]
                // Não retornou nenhum JOB
                if (arrayElements.length < 3) {
                    console.log('Result: 6')
                    return 6
                }
                // Se algum JOB falhar
                for (let index = 0; index < arrayElements.length; index++) {
                    const element = arrayElements[index]
                    const statusJob = await putJobsByUnit(element.ASRE_NM_JOB)
                    if (!statusJob) {
                        console.log('Result: 7')
                        return 7
                    }
                }

                // Se der falha na rotina de Gera pacote
                await delay(30000)
                const respGeraPacket = await jobGeraPacket()
                if (!respGeraPacket) {
                    console.log('Result: 8')
                    return 8
                }
                // Se o pacote não for gerado
                await delay(30000)
                const boolpacketGen = await getBoolPacketGenerated(unidadeDest)
                if (!boolpacketGen || !boolpacketGen.NUMPCKTS || boolpacketGen.NUMPCKTS < 1) {
                    console.log('Result: 9')
                    return 9
                }

                const respTroca = await execProcTrocaUnid(matricula, unidadeDest)
                if (!respTroca) {
                    console.log('Result: 10')
                    return 10
                }
            }
            return 0
        } catch (error) {
            return null
        }
    }
}

