import { TrocaService } from './troca.service'
import { NextFunction, Request, Response } from 'express'

export class TrocaController {
    public teste(): string {
        try {
            const trocaService: TrocaService = new TrocaService()
            return trocaService.resolveTroca()
        } catch (error) {
            console.log('==========Error controller===========')
            console.log(error)
            return 'error'
        }
    }
    public async getUsuario(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const trocaService: TrocaService = new TrocaService()
            const result = await trocaService.getUsuario(req.body.matricula, req.body.unidDest)
            return res
                .status(200)
                .json(result)
        } catch (error) {
            next(error)
        }
    }
}
