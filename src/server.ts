import appFactory from './app'
import * as express from 'express'


async function init() {
    try {
        const app: express.Application = await appFactory()
        app.listen(process.env.APP_PORT)
        console.log(`===========Server Running um port: ${process.env.APP_PORT} ==========`)
    } catch (error) {
        console.log('===========Erro init==========')
        console.log(error)
    }
}

init()
