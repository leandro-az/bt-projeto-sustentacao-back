
import { getConnection } from './mssql_conn'
import mssql from 'mssql'




export async function searchMatricula(matricula: number): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        const result: any = await sql.query(` SELECT ` +
            // `C.CLIE_NR_MATRICULA, ` +
            // `C.CLIE_NM_CLIENTE, ` +
            // `C.TICL_SQ_TIPO_CLIENTE, ` +
            // `CUE.CLIE_SQ_CLIENTE, ` +
            `U.UNID_CD_TAG AS \'UNIDADE\', ` +
            // `U.UNID_NM_FANTASIA, ` +
            `CUE.UNID_SQ_UNIDADE, ` +
            `CUE.CLUE_IN_PRINCIPAL ` +
            // `CUE.CLUE_IN_STATUS, ` +
            // `C.CLIE_DT_NASCIMENTO, ` +
            // `CUE.CLUE_DT_ATUALIZACAO, ` +
            // `CUE.CLUE_IN_ACESSO_REDE,  ` +
            // `CUE.ESTA_SQ_ESTADO,CLIE_NR_CPF ` +
            `FROM  ` +
            `CLIENTE_UNIDADE_ESTADO CUE ` +
            `INNER JOIN UNIDADE U ON U.UNID_SQ_UNIDADE = CUE.UNID_SQ_UNIDADE ` +
            `INNER JOIN CLIENTE C ON C.CLIE_SQ_CLIENTE = CUE.CLIE_SQ_CLIENTE ` +
            `WHERE  ` +
            `C.CLIE_NR_MATRICULA = ${matricula} `)
        sql.close()
        if (result.recordsets && result.recordsets.length) {
            const response: any[] = result.recordsets.map((element: any[]) => {
                return element.map(ele => {
                    return JSON.parse(JSON.stringify(ele))
                })
            })
            return response
        } else { return (null) }
    } catch (err) {
        console.log('==========error searchMatricula')
        console.log(err)
        return (err)
    }
}

export async function getLikedServe(unidadeDestino: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        // ${matricula}
        const result: any = await sql.query(`SELECT
        name 'LINKED_SERVER',
        RIGHT(name,5) 'TAG_UNDIADE'
        FROM sys.servers
        WHERE catalog = 'SGA_PRODUCAO'
        and RIGHT(name,5) = '${unidadeDestino}'`)
        sql.close()
        if (result.recordsets && result.recordsets.length) {
            const response: any[] = result.recordsets.map((element: any[]) => {
                return element.map(ele => {
                    return JSON.parse(JSON.stringify(ele))
                })
            })
            if (response[0] || ((response[0])[0])) {
                return ((response[0])[0])
            } else {
                return 0
            }
        } else { return (0) }
    } catch (err) {
        console.log('==========error getLikedServe')
        console.log(err)
        return (0)
    }



}

export async function updateDateUnidateDest(matricula: number, sqUnid: number, likedServ: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        const result: any = await sql.query(`UPDATE ${likedServ}.SGA_PRODUCAO.DBO.CLIENTE_UNIDADE_ESTADO ` +
            `SET CLUE_DT_ATUALIZACAO = GETDATE() ` +
            `WHERE CLIE_SQ_CLIENTE IN ` +
            `(SELECT C.CLIE_SQ_CLIENTE FROM CLIENTE_UNIDADE_ESTADO CUE ` +
            `JOIN CLIENTE C ON CUE.CLIE_SQ_CLIENTE = C.CLIE_SQ_CLIENTE ` +
            `JOIN UNIDADE U ON CUE.UNID_SQ_UNIDADE = U.UNID_SQ_UNIDADE ` +
            `WHERE CLIE_NR_MATRICULA = ${matricula} ` +
            `AND U.UNID_SQ_UNIDADE = ${sqUnid} ) ` +
            `AND UNID_SQ_UNIDADE = ${sqUnid} `)
        sql.close()
        return result.rowsAffected[0]
    } catch (err) {
        console.log('==========error updateDateUnidateDest')
        console.log(err)
        return (0)
    }
}


export async function getJobsByUnit(unidadeDestino: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        const result: any = await sql.query(`
        SELECT ASRE_NM_JOB FROM ASSINATURA_REPLICACAO AR
        INNER JOIN UNIDADE U ON AR.UNID_SQ_UNIDADE_PUBLICACAO = U.UNID_SQ_UNIDADE
        WHERE AR.PURE_NM_DATABASE_PUBLICACAO = 'SGA_PRODUCAO'
        AND U.UNID_CD_TAG = '${unidadeDestino}' `)
        sql.close()
        if (result.recordsets && result.recordsets.length) {
            const response: any[] = result.recordsets.map((element: any[]) => {
                return element.map(ele => {
                    return JSON.parse(JSON.stringify(ele))
                })
            })
            return response
        } else { return (null) }
    } catch (err) {
        console.log('==========error getJobsByUnit')
        console.log(err)
        return (err)
    }
}

export async function putJobsByUnit(jobName: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        await sql.query(` exec msdb.dbo.sp_start_job '${jobName}'`)
        sql.close()
        return 1
        // if (result.recordsets && result.recordsets.length) {
        //     const response: any[] = result.recordsets.map((element: any[]) => {
        //         return element.map(ele => {
        //             return JSON.parse(JSON.stringify(ele))
        //         })
        //     })
        //     if (response[0] || ((response[0])[0])) {
        //         return ((response[0])[0])
        //     } else {
        //         return 0
        //     }
        // } else { return (0) }
    } catch (err) {
        console.log('==========error putJobsByUnit')
        console.log(err)
        return (0)
    }
}

export async function jobGeraPacket(): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        await sql.query(` exec msdb.dbo.sp_start_job 'Job_Gera_Pacote_Replicacao_BT'`)
        sql.close()
        return 1
        // if (result.recordsets && result.recordsets.length) {
        //     const response: any[] = result.recordsets.map((element: any[]) => {
        //         return element.map(ele => {
        //             return JSON.parse(JSON.stringify(ele))
        //         })
        //     })
        //     if (response[0] || ((response[0])[0])) {
        //         return ((response[0])[0])
        //     } else {
        //         return 0
        //     }
        // } else { return (0) }
    } catch (err) {
        console.log('==========error jobGeraPacket')
        console.log(err)
        return (0)
    }
}


export async function getBoolPacketGenerated(unidadeDestino: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        // ${matricula}
        const result: any = await sql.query(`SELECT COUNT(U.UNID_CD_TAG) AS NUMPCKTS FROM PACOTE_REPLICACAO PARE WITH (NOLOCK)
        INNER JOIN UNIDADE U ON PARE.UNID_SQ_UNIDADE = U.UNID_SQ_UNIDADE
        WHERE PARE.PARE_IN_STATUS_PACOTE = 'F' -- FINALIZADO
        AND PARE.PARE_IN_PACOTE_DISPONIVEL_UNIDADE = 0 -- NAO PROCESSADO
        AND PARE.GRRE_SQ_GRUPO_REPLICACAO = 1
        AND UNID_CD_TAG='${unidadeDestino}'`)
        sql.close()
        if (result.recordsets && result.recordsets.length) {
            const response: any[] = result.recordsets.map((element: any[]) => {
                return element.map(ele => {
                    return JSON.parse(JSON.stringify(ele))
                })
            })
            if (response[0] || ((response[0])[0])) {
                return ((response[0])[0])
            } else {
                return 0
            }
        } else { return (0) }
    } catch (err) {
        console.log('==========error getBoolPacketGenerated')
        console.log(err)
        return (0)
    }
}

export async function execProcTrocaUnid(matricula: number, unidadeDestino: string): Promise<any> {
    try {
        const sql: mssql.ConnectionPool = await getConnection()
        // ${matricula}
        const result: any = await sql.query(`exec trocaUnidadePrincipal @MATRICULA ='${matricula}', @TAG_UNIDADE_DESTINO = '${unidadeDestino}'`)
        sql.close()
        return result.rowsAffected[0]
    } catch (err) {
        console.log('==========error execProcTrocaUnid')
        console.log(err)
        return (0)
    }
}
