import mssql from 'mssql'

export async function getConnection(): Promise<mssql.ConnectionPool> {
    const config = {
        server: process.env.mssql_host!,
        user: process.env.mssql_user!,
        password: process.env.mssql_password!,
        database: process.env.mssql_database!,
        requestTimeout: 300000
    }
    return new mssql.ConnectionPool(config).connect()
}
