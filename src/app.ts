import express from 'express'
import * as bodyParser from 'body-parser'
import trocaRouter from './Troca/troca.router'


class App {
    public express: express.Application = express()
    public async initialize() {
        try {
            this.express.use(bodyParser.json())
            // talvez usaremos this.express.use(cors())
            this.routes()
        } catch (error) {
            console.log('==============Initializer error==========')
            console.log(error)
        }
    }

    private routes(): void {
        this.express.use(trocaRouter)
        this.express.all('*', async (_req, res) => {
            res.status(404).json('not found')
        })
    }
}

export default async function appFactory() {
    const app = new App()
    await app.initialize()
    return app.express
}
